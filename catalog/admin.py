#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline

from .models import *

class CatalogItemAdmin(admin.ModelAdmin):
    ordering = ['order',]
    list_display = ('title', 'geo_coordinate_lng', 'geo_coordinate_lat', )
    list_editable = ('geo_coordinate_lng', 'geo_coordinate_lat', )

admin.site.register(CatalogItem, CatalogItemAdmin)