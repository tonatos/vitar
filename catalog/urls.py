#-*- coding: UTF-8 -*-

from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', CatalogListView.as_view()),

    url(r'^(?P<pk>\d+)/$', CatalogItemlDetailView.as_view(),
        name='catalog_item'),
)