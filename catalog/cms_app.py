#-*- coding: UTF-8 -*-
from django.conf.urls import patterns, url

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import CatalogMenu

class CatalogApphook(CMSApp):
    name = u'Объекты'
    urls = ['catalog.urls']
    menus = [CatalogMenu]

apphook_pool.register(CatalogApphook)