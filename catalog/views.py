#-*- coding: UTF-8 -*-
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.views.generic.base import ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormMixin
from django.views.generic.list import ListView
from django.contrib.auth.models import User

from .forms import *
from .models import *

class CatalogListView(ListView):
    model = CatalogItem
    context_object_name='items'
    template_name = 'catalog/list.html'
    paginate_by = 30

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class CatalogItemlDetailView(DetailView):
    model = CatalogItem
    context_object_name='item'
    template_name = 'catalog/item.html'


    def get_context_data(self, *args, **kwargs):
        context = super(CatalogItemlDetailView, self).get_context_data(*args, **kwargs)
        #context['form'] = RequestForm()
        return context
