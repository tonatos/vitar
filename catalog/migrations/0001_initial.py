# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CatalogItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u0435 \u043d\u043e\u0432\u043e\u0441\u0442\u0438')),
                ('description', models.TextField(max_length=255, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435', blank=True)),
                ('content', djangocms_text_ckeditor.fields.HTMLField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435', blank=True)),
                ('is_ekb', models.BooleanField(default=False, help_text='\u041d\u0443\u0436\u043d\u043e \u0434\u043b\u044f \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043d\u0430 \u043a\u0430\u0440\u0442\u0435', verbose_name='\u042d\u0442\u043e \u0415\u043a\u0430\u0442\u0435\u0440\u0438\u043d\u0431\u0443\u0440\u0433?')),
                ('geo_coordinate_lng', models.CharField(max_length=50, verbose_name='\u0413\u0435\u043e\u043a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u044b (\u0448\u0438\u0440\u043e\u0442\u0430)', blank=True)),
                ('geo_coordinate_lat', models.CharField(max_length=50, verbose_name='\u0413\u0435\u043e\u043a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u044b (\u0434\u043e\u043b\u0433\u043e\u0442\u0430)', blank=True)),
                ('order', models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438')),
            ],
            options={
                'verbose_name': '\u041e\u0431\u044a\u0435\u043a\u0442',
                'verbose_name_plural': '\u041e\u0431\u044a\u0435\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
    ]
