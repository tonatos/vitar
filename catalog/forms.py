# -*- coding: utf-8 -*-
from django import forms
from .models import Request

class RequestForm(forms.ModelForm):
    title = forms.CharField(
        label = u'Ваше имя',
        widget = forms.widgets.Input(attrs={'class': 'form-control', 'type': 'text'}),
        required = True )

    phone = forms.CharField(
        label = u'Телефон',
        widget = forms.widgets.Input(attrs={'class': 'form-control', 'type': 'text'}),
        required = True )

    email = forms.CharField(
        label = u'Эл. почта',
        widget = forms.widgets.Input(attrs={'class': 'form-control', 'type': 'text'}),
        required = False )

    subject = forms.CharField(
        label = u'Сообщение',
        widget = forms.widgets.Textarea(attrs={'class': 'form-control', 'rows': 4}),
        required = False )

    class Meta:
        fields = [
            'title',
            'phone',
            'email',
            'subject',
        ]
        model = Request
        exclude = ('created', 'updated', )

    def __init__(self, *args, **kwargs):
        super(RequestForm, self).__init__(*args, **kwargs)

