#-*- coding: UTF-8 -*-
from django import template
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from catalog.forms import RequestForm
from catalog.models import CatalogItem

register = template.Library()

def __get_coord(vars, obj):
    dlt_left = (float(vars['start_lat']) - float(vars['end_lat']))
    dlt_top = (float(vars['start_lng']) - float(vars['end_lng']))

    try:
        pxl_left = (vars['width'] * (vars['start_lat'] - float(obj.geo_coordinate_lat))) / dlt_left
        pxl_top = (vars['height'] * (vars['start_lng'] - float(obj.geo_coordinate_lng))) / dlt_top
    
        return vars['left'] + pxl_left, vars['top'] + pxl_top
    except (ValueError):
        return 0, 0


@register.filter('get_city_coord')
def city_coord_filter(value):
    vars = {
        'left': 130,
        'top': 350,

        'width': 760.0,
        'height': 700.0,

        'start_lng': 56.941295, # долгота
        'end_lng': 56.719082,

        'start_lat': 60.321451, # широта
        'end_lat': 60.856348,
    }
    left, top = __get_coord(vars, value)
    return "left: %spx; top: %spx;" % (left, top)

@register.filter('get_region_coord')
def region_coord_filter(value):
    vars = {
        'left': 73,
        'top': 72,

        'width': 760.0,
        'height': 910.0,

        'start_lng': 61.923532, # долгота
        'end_lng': 56.059876,

        'start_lat': 57.257871, # широта
        'end_lat': 66.178769,
    }
    left, top = __get_coord(vars, value)
    return "left: %spx; top: %spx;" % (left, top)

@register.inclusion_tag('catalog/tags/map.html')
def get_map(request, index=True, exclude=None):
    objects_region = CatalogItem.objects.filter(is_ekb=False)
    objects_city = CatalogItem.objects.filter(is_ekb=True)

    if request.method == 'POST':
        feedback_form = RequestForm(data = request.POST)

        if feedback_form.is_valid():
            ask = feedback_form.save(commit = False)
            ask.save()

            try:
                email = User.objects.filter(is_superuser = True)[0].email
            except ValueError:
                email = settings.MAIN_EMAIL

            send_mail(
                u'Сообщение с сайта «Molintego»',
                render_to_string(
                    'email/email_feedback_form.txt',
                    {
                        'ask': ask,
                        'url': request.build_absolute_uri(
                            reverse('admin:vitar_feedbacksubject_change', args=[ask.pk])
                        )
                    }
                ), 'no-reply@vitar-s.ru', settings.EMAILS + [email]
            )
            return {
                'form': RequestForm(),
                'ok': True,
                'modifier': 'we-map_inner' if not index else '',
                'objects_city': objects_city,
                'objects_region': objects_region
            }
    else:
        feedback_form = RequestForm()

    return {
        'request': request,
        'form': feedback_form,
        'modifier': 'we-map_inner' if not index else '',
        'objects_city': objects_city,
        'objects_region': objects_region
    }