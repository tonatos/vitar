#-*- coding: UTF-8 -*-

import datetime

from django.db import models

# Create your models here.
from djangocms_text_ckeditor.fields import HTMLField


class CatalogItem(models.Model):
    title = models.CharField(u'Заголовое новости', max_length=255)

    description = models.TextField(u'Краткое описание', blank=True, max_length=255)
    
    content = HTMLField(u'Содержимое', blank=True)

    is_ekb = models.BooleanField(u'Это Екатеринбург?', default=False,
                 help_text=u'Нужно для отображения на карте')

    geo_coordinate_lng = models.CharField(
        u'Геокоординаты (долгота)',
        max_length=50,
        blank=True)

    geo_coordinate_lat = models.CharField(
        u'Геокоординаты (широта)',
        max_length=50,
        blank=True)

    order = models.PositiveIntegerField(u'Порядок сортировки', default=0, blank=False, null=False)

    @models.permalink
    def get_absolute_url(self):
        return 'catalog_item', [self.pk]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Объект'
        verbose_name_plural = u'Объекты'


class Request(models.Model):
    title = models.CharField(u'Ваше имя', max_length=255)
    phone = models.CharField(u'Телефон', max_length=255)
    email = models.EmailField(u'E-mail', max_length=255)
    subject = models.TextField(u'Сообщение', max_length=1000)

    created = models.DateTimeField(u'Дата добавления',
                                   auto_now=False, auto_now_add=True, null=True,
                                   blank = False, default = datetime.datetime.now)

    updated = models.DateTimeField(u'Дата изменения', auto_now=True, auto_now_add=True,
                                   blank = False, default = datetime.datetime.now)

    item = models.ForeignKey(CatalogItem, blank=True, null=True)

    def __unicode__(self):
        return str(self.created)

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'
        ordering = ['-created', ]