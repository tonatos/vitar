#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline

from .models import *

class NewsItemAdmin(admin.ModelAdmin):
    ordering = ['-created',]
    list_display = ('title', 'created',)

admin.site.register(NewsItem, NewsItemAdmin)