#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *

class NewsItemListView(ListView):
    model = NewsItem
    context_object_name='items'
    template_name = 'news/list.html'
    paginate_by = 20

    def get_context_data(self, *args, **kwargs):
        import datetime
        context = super(NewsItemListView, self).get_context_data(*args, **kwargs)

        context['years'] = []
        for i in xrange(2015, datetime.datetime.now().year + 1):
            context['years'].append(i)
        context['years'].reverse()

        context['month'] = [
            [1, 'Январь'],
            [2, 'Февраль'],
            [3, 'Март'],
            [4, 'Апрель'],
            [5, 'Май'],
            [6, 'Июнь'],
            [7, 'Июль'],
            [8, 'Август'],
            [9, 'Сентябрь'],
            [10, 'Октябрь'],
            [11, 'Ноябрь'],
            [12, 'Декабрь'],]

        context['now_year'] = datetime.datetime.now().year
        context['max_month'] = datetime.datetime.now().month

        context['current_year'] = self.kwargs.get('year', None)
        context['current_month'] = self.kwargs.get('month', None)

        return context

    def get_queryset(self):
        return super(NewsItemListView, self).get_queryset().order_by('-created')

class NewsItemListByYearView(NewsItemListView):
    def get_queryset(self):
        return super(NewsItemListByYearView, self).get_queryset()\
                .filter(created__year=self.kwargs['year'])

class NewsItemListByMonthView(NewsItemListView):
    def get_queryset(self):
        import datetime

        y = int(self.kwargs['year'])
        m = int(self.kwargs['month'])

        start_date = datetime.date(y, m, 1)

        for i in xrange(28, 32):
            try:
                end_date = datetime.date(y, m, i)
            except ValueError:
                break

        return super(NewsItemListByMonthView, self).get_queryset()\
                .filter(created__range=(start_date, end_date))


class NewsItemDetailView(DetailView):
    model = NewsItem
    context_object_name = 'item'
    template_name = 'news/item.html'