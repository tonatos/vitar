#-*- coding: UTF-8 -*-

from datetime import datetime

from django.db import models
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField

class NewsItem(models.Model):
    title = models.CharField(u'Заголовое новости', max_length=150)

    content = HTMLField(u'Содержимое', blank=True)

    created = models.DateTimeField(u'Дата добавления', auto_now=False,
           null=True, blank = False, default = datetime.now)

    updated = models.DateTimeField(u'Дата изменения', auto_now=True,
           auto_now_add=True, blank = False, default = datetime.now)

    @models.permalink
    def get_absolute_url(self):
        return 'news_item', [self.pk]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'
        ordering = ['-created']