#-*- coding: UTF-8 -*-
from django import template

register = template.Library()

@register.inclusion_tag('news/tags/filter.html', takes_context=True)
def news_filter(context, exclude=None):
    return { 'context': context }
