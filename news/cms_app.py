#-*- coding: UTF-8 -*-
from django.conf.urls import patterns, url

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import NewsMenu

class NewsApphook(CMSApp):
    name = u'Новости'
    urls = ['news.urls']
    menus = [NewsMenu]

apphook_pool.register(NewsApphook)