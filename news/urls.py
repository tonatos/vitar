from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', NewsItemListView.as_view(), name="news"),

    url(r'^(?P<year>\d{4})/$', NewsItemListByYearView.as_view(),
        name="news_by_year"),

    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/$', NewsItemListByMonthView.as_view(),
        name="news_by_month"),

    url(r'^news-(?P<pk>\d+)/$', NewsItemDetailView.as_view(),
        name='news_item'),
)
