$(document).ready(function(){
    $('.we-map__map-region-city').click(function(){
        $('.we-map__popover').fadeOut(100, function(){
            $(this).remove();
        });
        $('.we-map__map-city').addClass('showed');
        return false;
    });

    $('.we-map__map-city-close').click(function(){
        $('.we-map__map-city').removeClass('showed');
        $('.we-map__popover').fadeOut(100, function(){
            $(this).remove();
        });
        $('.we-map__map-markers-item').removeClass('active');
        return false;
    });

    $('.we-map__map-markers-item').click(function(){
        $flag = $(this);
        $popover = $('<div class="we-map__popover"><a class="close"></a><h4></h4><p></p><a href="#">Узнать подробнее</a></div>');

        if ( $('.we-map__popover').length > 0 ) {
            $('.we-map__popover').fadeOut(100, function(){
                $(this).remove();
            });
            $flag.removeClass('active');
            return;
        }

        $flag.addClass('active');

        $popover
            .prependTo('body')
            .fadeIn(100)
            .find('h4').text($flag.data('title'))
            .parent()
            .find('p').text($flag.data('description'))
            .parent()
            .find('a').attr('href', $flag.data('link'))
            .parent()
            .css({
                'top': $flag.offset().top - $popover.outerHeight() - 10,
                'left': $flag.offset().left - 178,
            })
            .ready(function(){
                $('.we-map__popover .close').click(function(){
                    $('.we-map__popover').fadeOut(100, function(){
                        $flag.removeClass('active');
                        $(this).remove();
                    });
                    return false;
                })
            });
    });

    $('.accordeon > .item > .title').click(function(){
        var $item = $(this).parents('.item');
        var $accordeon = $item.parent();

        $accordeon.find('.item').removeClass('active');
        $item.addClass('active');
    });

    $('#show-request-form').click(function(){
        $(this).addClass('clicked');
        $('.request-form').addClass('active');
        return false;
    })
})
