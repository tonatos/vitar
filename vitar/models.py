#-*- coding: UTF-8 -*-
import os
from datetime import datetime

from django.db import models

class FeedbackSubject(models.Model):
    name = models.CharField(u'Ваше имя', max_length=255)
    phone = models.CharField(u'Телефон', max_length=255)
    email = models.EmailField(u'E-mail', max_length=255, blank=True)
    subject = models.TextField(u'Текст сообщения', max_length=1000, blank=True)

    created = models.DateTimeField(u'Дата добавления', auto_now=False, auto_now_add=True, null=True, blank = False, default = datetime.now)
    updated = models.DateTimeField(u'Дата изменения', auto_now=True, auto_now_add=True, blank = False, default = datetime.now)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Сообщение'
        verbose_name_plural = u'Сообщения'
        ordering = ['-created', ]