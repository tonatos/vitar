# -*- coding: utf-8 -*-
from django import forms
from .models import FeedbackSubject

class FeedbackSubjectForm(forms.ModelForm):
    name = forms.CharField(
        label = 'Как к вам обращаться',
        widget = forms.widgets.Input(attrs={'class': 'form-control', 'placeholder': u'Как к вам обращаться'}),
        required = True )

    phone = forms.CharField(
        label = 'Телефон',
        widget = forms.widgets.Input(attrs={'class': 'form-control', 'placeholder': u'Телефон'}),
        required = True )

    email = forms.CharField(
        label = 'Эл. почта',
        widget = forms.widgets.Input(attrs={'class': 'form-control', 'placeholder': u'Эл. почта'}),
        required = False )

    subject = forms.CharField(
        label = 'Сообщение',
        widget = forms.widgets.Textarea(attrs={'class': 'form-control', 'rows': 3, 'placeholder': u'Текст сообщения'}),
        required = False )

    class Meta:
        fields = [
            'name',
            'phone',
            'email',
            'subject',
        ]
        model = FeedbackSubject
        exclude = ('created', 'updated', )

    def __init__(self, *args, **kwargs):
        super(FeedbackSubjectForm, self).__init__(*args, **kwargs)
