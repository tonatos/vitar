#-*- coding: UTF-8 -*-
from django import template
from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from vitar.forms import FeedbackSubjectForm
from feedback.models import ClientFeedback
from news.models import NewsItem

register = template.Library()

@register.inclusion_tag('vitar/tags/feedback.html')
def feedback_index(exclude=None):
    return {'feedback': ClientFeedback.objects.all()[:4]}

@register.inclusion_tag('vitar/tags/news.html')
def news_index(exclude=None):
    return {'news': NewsItem.objects.all()[:4]}

@register.inclusion_tag('vitar/tags/feedback_form.html')
def feedback_form(request, exclude=None):
    if request.method == 'POST':
        feedback_form = FeedbackSubjectForm(data = request.POST)

        if feedback_form.is_valid():
            ask = feedback_form.save(commit = False)
            ask.save()

            try:
                email = User.objects.filter(is_superuser = True)[0].email
            except ValueError:
                email = settings.MAIN_EMAIL

            send_mail(
                u'Сообщение с сайта «Molintego»',
                render_to_string(
                    'email/email_feedback_form.txt',
                    {
                        'ask': ask,
                        'url': request.build_absolute_uri(
                            reverse('admin:vitar_feedbacksubject_change', args=[ask.pk])
                        )
                    }
                ), 'no-reply@vitar-s.ru', settings.EMAILS + [email]
            )
            return {
                'form': FeedbackSubjectForm(),
                'request': request,
                'ok': True
            }
    else:
        feedback_form = FeedbackSubjectForm()

    return {
        'form': feedback_form,
        'request': request
    }

