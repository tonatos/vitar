#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline

from .models import *

class FeedbackSubjectAdmin(admin.ModelAdmin):
    ordering = ['created',]
    list_display = ('name', 'created',)

admin.site.register(FeedbackSubject, FeedbackSubjectAdmin)