from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', SolutionItemListView.as_view()),

    url(r'^(?P<pk>\d+)/$', SolutionItemDetailView.as_view(),
        name='solution_item'),
)
