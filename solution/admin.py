#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline

from .models import *

class SolutionItemAdmin(admin.ModelAdmin):
    list_display = ('title',)

admin.site.register(SolutionItem, SolutionItemAdmin)