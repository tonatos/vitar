#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *

class SolutionItemListView(ListView):
    model = SolutionItem
    context_object_name='items'
    template_name = 'solution/solution-list.html'



class SolutionItemDetailView(DetailView):
    model = SolutionItem
    context_object_name='item'
    template_name = 'solution/item.html'