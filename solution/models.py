#-*- coding: UTF-8 -*-

from django.db import models
from filer.fields.image import FilerImageField
from djangocms_text_ckeditor.fields import HTMLField

# Create your models here.

class SolutionItem(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)

    description = models.TextField(u'Описание', blank=True)

    content = HTMLField(u'Содержимое', blank=True)

    order = models.PositiveIntegerField(u'Порядок сортировки', default=0, blank=False, null=False)

    @models.permalink
    def get_absolute_url(self):
        return 'solution_item', [self.id]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Услуга'
        verbose_name_plural = u'Услуги'
        ordering=('order',)
