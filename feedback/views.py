#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *

class ClientFeedbackListView(ListView):
    model = ClientFeedback
    context_object_name='items'
    template_name = 'feedback/list.html'



class ClientFeedbackDetailView(DetailView):
    model = ClientFeedback
    context_object_name='item'
    template_name = 'feedback/item.html'