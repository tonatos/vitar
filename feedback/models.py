#-*- coding: UTF-8 -*-

from django.db import models

from filer.fields.image import FilerImageField

# Create your models here.
class ClientFeedback(models.Model):
    title = models.CharField(u'Название компании', max_length=150)

    content = models.TextField(u'Текст отзыва', blank=True)

    img =  FilerImageField(verbose_name=u'Изображение',
                           related_name="catalog_img")

    object_link = models.URLField(u'Ссылка на объект в портфолио', blank=True, null=True)

    order = models.PositiveIntegerField(u'Порядок сортировки', default=0, blank=False, null=False)

    @models.permalink
    def get_absolute_url(self):
        return 'feedback_item', [self.pk]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'
        ordering = ['order']