#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline

from .models import *

class ClientFeedbackAdmin(admin.ModelAdmin):
    ordering = ['order',]
    list_display = ('title',)

admin.site.register(ClientFeedback, ClientFeedbackAdmin)