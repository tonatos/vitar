from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', ClientFeedbackListView.as_view()),

    url(r'^(?P<pk>\d+)/$', ClientFeedbackDetailView.as_view(),
        name='feedback_item'),
)
